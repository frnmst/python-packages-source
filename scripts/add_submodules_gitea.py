#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# add_submodules_gitea.py
#
# Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of python-packages-source.
#
# python-packages-source is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-packages-source is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-packages-source.  If not, see <http://www.gnu.org/licenses/>.
r"""Add git submodules to the repository."""

import contextlib
import os
import shlex

import fpyutils
import gitea

SUBMODULES_DIRECTORY = '../submodules'


# See
# https://stackoverflow.com/a/13847807
# CC BY-SA 4.0
# spiralman, bryant1410
@contextlib.contextmanager
def _pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)


if __name__ == '__main__':

    def main():
        r"""Run main."""
        results = gitea.get_org_repos()

        # Get the original URLs of mirrors only.
        with _pushd(SUBMODULES_DIRECTORY):
            for r in results:
                for rr in r:
                    if rr['mirror'] and not rr['empty']:
                        fpyutils.shell.execute_command_live_output(
                            'git submodule add ' +
                            shlex.quote(rr['clone_url']))

    main()
