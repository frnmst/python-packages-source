#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# collect_data.py
#
# Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of python-packages-source.
#
# python-packages-source is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-packages-source is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-packages-source.  If not, see <http://www.gnu.org/licenses/>.
r"""Get the data to use in the stats."""

import csv
import datetime
import pathlib
import re

import gitea

OUTPUT_FILE = '../stats/repo_stats.csv'
REGEX = r'http(|s)://(|www\.)github.com/.*/.*'

# Collect data max one every 86400 seconds by default.
PLOT_DAYS_SENSIBILITY = 1

if __name__ == '__main__':

    def main():
        r"""Run main."""
        now = datetime.datetime.now(datetime.timezone.utc)

        results = gitea.get_org_repos()
        if pathlib.Path(OUTPUT_FILE).is_file():
            total, match, date, runs = gitea.read_csv(OUTPUT_FILE)
            date = max(date)
        else:
            date = now - datetime.timedelta(days=1)

        if (now - date).days >= PLOT_DAYS_SENSIBILITY:
            date = now.strftime('%Y-%m-%d %H:%M:%S %z')

            total = 0
            match = 0
            for r in results:
                total += len(r)
                for rr in r:
                    if rr['mirror'] and not rr['empty']:
                        if re.match(REGEX, rr['original_url']):
                            match += 1

            gitea.write_csv(OUTPUT_FILE, total, match, date)

    main()
