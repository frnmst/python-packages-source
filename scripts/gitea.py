#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# gitea.py
#
# Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of python-packages-source.
#
# python-packages-source is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-packages-source is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-packages-source.  If not, see <http://www.gnu.org/licenses/>.
r"""Common functions."""

import csv
import datetime
import json
import os
import urllib

import requests

GITEA_BASE_URL = 'software.franco.net.eu.org'
GITEA_ORG = 'mirrors-python'


def get_org_repos() -> list:
    r"""Get a list of objects containing repositories."""
    results = list()
    go = True

    i = 1
    while go:
        # See also
        # https://try.gitea.io/api/swagger#/organization/orgListRepos
        # for Gitea's API.
        args = {'page': str(i)}
        parse = urllib.parse.ParseResult(scheme='https',
                                         netloc=GITEA_BASE_URL,
                                         path='api/v1/orgs/' + GITEA_ORG +
                                         '/repos',
                                         params='',
                                         query=urllib.parse.urlencode(args),
                                         fragment='')
        repos = urllib.parse.urlunparse(parse)
        # print(repos)
        r = requests.get(repos)

        if json.loads(r.text) == list():
            # No more repos: stop condition.
            go = False
        else:
            results.append(json.loads(r.text))

        i += 1

    return results


def read_csv(file: str) -> tuple:
    r"""Read a CSV file."""
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        total = list()
        match = list()
        date = list()

        # Add a new value: a list of incrementing integers
        # representing the run number.
        runs = list()
        i = 1
        for row in reader:
            total.append(int(row[0]))
            match.append(int(row[1]))
            date.append(
                datetime.datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S %z'))
            runs.append(i)
            i += 1

    return total, match, date, runs


def write_csv(file: str, total: list, match: list, date: list):
    r"""Write a CSV file."""
    with open(file, 'a') as csvfile:
        writer = csv.writer(csvfile,
                            delimiter=',',
                            quotechar='|',
                            quoting=csv.QUOTE_MINIMAL)
        writer.writerow([total, match, date])
