#!/usr/bin/env bash
#
# add_submodules.sh
#
# Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of python-packages-source.
#
# python-packages-source is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-packages-source is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-packages-source.  If not, see <http://www.gnu.org/licenses/>.

# Usage: ./add_submodules.sh [GIT_URL] [GIT_URL] ...

pushd ../submodules
for url in "$@"; do
    # Always add '.git'
    if [ "${url}" = "${url%%.git}" ]; then
        url=""${url}".git"
    fi

    echo "Adding "${url}""
    git submodule add "${url}"
done
popd
