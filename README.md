# Python packages source

Git repository pointers and configurations to build Python packages from source.

## Table of contents

<!--TOC-->

- [Python packages source](#python-packages-source)
  - [Table of contents](#table-of-contents)
  - [Description](#description)
  - [Build script](#build-script)
  - [Submodules](#submodules)
    - [Important](#important)
    - [Comments](#comments)
      - [Instructions](#instructions)
        - [python-cffi](#python-cffi)
        - [cryptography](#cryptography)
        - [graphviz](#graphviz)
        - [babel](#babel)
        - [lxml](#lxml)
      - [Problematic repositories](#problematic-repositories)
  - [Client configuration](#client-configuration)
    - [PIP](#pip)
    - [Pipenv](#pipenv)
      - [shell](#shell)
      - [Pipfile](#pipfile)
      - [Environment variable](#environment-variable)
  - [License](#license)
  - [Changelog and trusted source](#changelog-and-trusted-source)
  - [Crypto donations](#crypto-donations)

<!--TOC-->

## Description

This is a meta-repository which contains pointers (git submodules)
used to build Python 3 packages as an alternative to [PyPI](https://pypi.org/).

Git submodules point to mirrors on this server rather than upstream repositories.
Using mirrors enables to work offline: the source of most package dependencies
I use are hosted on GitHub.

![graph](https://assets.franco.net.eu.org/image/71d0faf829f9d17f8fa3b4b4032b8e034800ef01_graph0.png)

These numbers include all repositories hosted on
[mirrors-python](https://software.franco.net.eu.org/mirrors-python).
I don't use all these packages but I mirrored them just in case.

You can imagine what happens when GitHub is down...

## Build script

Most packages present on [pypi.franco.net.eu.org](https://pypi.franco.net.eu.org/)
have been built using the
[build_python_packages.py](https://docs.franco.net.eu.org/ftutorials/en/content/server/package-mirroring.html#pypi-server)
script.

## Submodules

1. clone this repository normally (without recrursive submodules)

   ```shell
   git clone https://software.franco.net.eu.org/frnmst/python-packages-source.git
   ```

2. to add submodules from external repositories run

   ```shell
   make submodules-add SUBMODULES="{submodule_URL_0} ${submodule_URL_1} ... ${submodule_URL_n}"
   ```

3. to add all submodules from a Gitea organization through Gitea's API run

   ```shell
   make submodules-add-gitea
   ```

   https://software.franco.net.eu.org/mirrors-python is hardcoded in the script as variables

4. to update all submodules in the index run

   ```shell
   make submodules-update
   ```

### Important

- https://stackoverflow.com/questions/4873980/git-diff-says-subproject-is-dirty
- https://newbedev.com/how-do-i-move-an-existing-git-submodule-within-a-git-repository

### Comments

#### Instructions

##### python-cffi

- see [Installation and Status](https://cffi.readthedocs.io/en/latest/installation.html)

  ```shell
  apt-get install libffi-dev python3-dev
  ```

##### cryptography

- see [Installation -> Rust](https://cryptography.io/en/latest/installation/#rust) and [this stackoverflow answer](https://stackoverflow.com/a/22210069)

  ```shell
  apt-get install build-essential libssl-dev libffi-dev python3-dev rustc cargo`
  ```

##### graphviz

- see [Installation](https://graphviz.readthedocs.io/en/stable/manual.html#installation)

  ```shell
  apt-get install graphviz libgraphviz-dev
  ```

##### babel

- see [installation -> Living on the Edge](http://babel.pocoo.org/en/latest/installation.html#living-on-the-edge)

  ```shell
  pip3 install pytz
  python3 setup.py import_cldr
  ```

##### lxml

- see [How to build lxml from source](https://lxml.de/build.html)

#### Problematic repositories

- `kiwi`
- `lxml`
- `matplotlib`
- `numpy`

## Client configuration

### PIP

Add this to `~/.config/pip/pip.conf`

```ini
[global]
timeout = 60
index-url = https://pypi.franco.net.eu.org/simple
```

### Pipenv

#### shell

```shell
pipenv install --pypi-mirror https://pipy.franco.net.eu.org/simple
```

#### Pipfile

Use this as source in the Pipfile:

```ini
[[source]]
name = "pypi"
url = "https://pypi.franco.net.eu.org/simple/"
verify_ssl = true
```

#### Environment variable

This is my preferred method. Add the following to your
shell configuration or profile:

```shell
export PIPENV_PYPI_MIRROR=https://pypi.franco.net.eu.org/simple
```

## License

Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)

python-packages-source is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

python-packages-source is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-packages-source.  If not, see <http://www.gnu.org/licenses/>.

## Changelog and trusted source

You can check the authenticity of new releases using my public key.

Changelogs, instructions, sources and keys can be found at [blog.franco.net.eu.org/software](https://blog.franco.net.eu.org/software/).

## Crypto donations

- Bitcoin: `bc1qnkflazapw3hjupawj0lm39dh9xt88s7zal5mwu`
- Monero: `84KHWDTd9hbPyGwikk33Qp5GW7o7zRwPb8kJ6u93zs4sNMpDSnM5ZTWVnUp2cudRYNT6rNqctnMQ9NbUewbj7MzCBUcrQEY`
- Dogecoin: `DMB5h2GhHiTNW7EcmDnqkYpKs6Da2wK3zP`
- Vertcoin: `vtc1qd8n3jvkd2vwrr6cpejkd9wavp4ld6xfu9hkhh0`
